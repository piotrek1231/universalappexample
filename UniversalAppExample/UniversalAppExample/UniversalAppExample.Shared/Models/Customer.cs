﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;

namespace UniversalAppExample.Models
{
    public class Customer : Person
    {
        [MaxLength(10)]
        public string CreditCardNumber { get; set; }
        public int LoyaltyPoints { get; set; }
    }
}
