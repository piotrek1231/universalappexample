﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using UniversalAppExample.Infrastructure.Resources;
using UniversalAppExample.Interfaces;
using GalaSoft.MvvmLight.Command;
using UniversalAppExample.Infrastructure;
using UniversalAppExample.Models;
using UniversalAppExample.Models.Relationships;
using UniversalAppExample.Views;

namespace UniversalAppExample.ViewModels
{
    public class StartPageViewModel : ViewModelBase
    {
        INavigationService _navigationService;
        public StartPageViewModel(INavigationService navigationService)
        {
            _navigationService = navigationService;
            NavigateToMainPageCommand = new RelayCommand(() => NavigateToMainPage(), () => true);

            //CheckDatabase();
        }


        private async void CheckDatabase()
        {
            if (await SQLiteFacade.InitializeDatabase())
            {

                List<Invoice> invoices = new List<Invoice>();
                invoices.Add(new Invoice());
                invoices.Add(new Invoice());

                invoices[0].Seller = new Seller() {Forename = "111dsfsdfsdf", Surname = "fdsfsdfsdf", DateOfStartWork = new DateTime(1990,3,3)};
                invoices[1].Seller = new Seller() { Forename = "222dsfsdfsdf", Surname = "fdsfsdfsdf", DateOfStartWork = new DateTime(1990, 3, 3) };

                
                List<Product> products = new List<Product>();
                products.Add(new Product() {Name = "Product 1"});
                products.Add(new Product() { Name = "Product 2" });
                products.Add(new Product() { Name = "Product 3" });
                products.Add(new Product() { Name = "Product 4" });
                products.Add(new Product() { Name = "Product 5" });
                products.Add(new Product() { Name = "Product 6" });

                invoices[0].Products = new List<Product>();
                invoices[0].Products.Add(products[0]);
                invoices[0].Products.Add(products[1]);
                invoices[0].Products.Add(products[2]);

                invoices[1].Products = new List<Product>();
                invoices[1].Products.Add(products[4]);
                invoices[1].Products.Add(products[5]);
                

                SQLiteFacade.AddInvoices(invoices);
                var readed = SQLiteFacade.GetInvoices();

                /*
                List<Seller> sellers = new List<Seller>();
                sellers.Add(new Seller() { Forename = "Adam", Surname = "Kowalski" });
                sellers.Add(new Seller() { Forename = "Anna", Surname = "Lewandowska" });

                SQLiteFacade.AddSellers(sellers);

                 */



            } 
        }



        #region Commands

        public ICommand NavigateToMainPageCommand { get; private set; }

        private void NavigateToMainPage()
        {
            _navigationService.Navigate(typeof(MainPageView)); 
        }

        #endregion

        #region Getters and Setters

            #region Localization

            public string ApplicationName
            {
                get
                {
                    return Localization.GetString(LocalizedStringEnum.ApplicationName);
                }
            }

            public string HeaderText
            {
                get
                {
                    return Localization.GetString(LocalizedStringEnum.StartPageHeaderText);
                }
            }

            public string WelcomeMessage
            {
                get
                {
                    return Localization.GetString(LocalizedStringEnum.StartPageWelcomeMessage);
                }
            }

            public string NavigateToMainPageButton
            {
                get
                {
                    return Localization.GetString(LocalizedStringEnum.StartPageAppBarButtonLabelNavigateToMainPage);
                }
            }

            #endregion

        #endregion


    }
}
