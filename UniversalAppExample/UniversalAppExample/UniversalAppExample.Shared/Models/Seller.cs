﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;
using SQLiteNetExtensions.Attributes;

namespace UniversalAppExample.Models
{
    public class Seller : Person
    {
        [Unique]
        public string InternalId { get; set; }
        public DateTime DateOfStartWork { get; set; }
    }
}
