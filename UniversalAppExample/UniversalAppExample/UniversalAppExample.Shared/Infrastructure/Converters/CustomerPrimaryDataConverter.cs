﻿using System;
using System.Collections.Generic;
using System.Text;
using Windows.UI.Xaml.Data;
using UniversalAppExample.Models;

namespace UniversalAppExample.Infrastructure.Converters
{
    public sealed class CustomerPrimaryDataConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            if (value == null)
                return null;

            try
            {
                Customer customer = (Customer) value;

                return String.Format(customer.Forename + " " + customer.Surname);
            }
            catch (Exception)
            {
                return value.ToString();
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}
