﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;

namespace UniversalAppExample.Infrastructure.Data.Local
{
    public static class DatabaseHelper
    {
        private static Windows.Storage.StorageFolder localFolder = Windows.Storage.ApplicationData.Current.LocalFolder;

        public static async Task<string> GetPathOfDatabaseInstance(string databaseName)
        {
            if (!await DatabaseExistInRoamingFolder(databaseName))
            {
                StorageFile sampleFile = await localFolder.CreateFileAsync(databaseName, CreationCollisionOption.ReplaceExisting);
            }

            StorageFile database = await localFolder.GetFileAsync(databaseName);
            return database.Path;
        }

        private static async Task<bool> DatabaseExistInRoamingFolder(string databaseName)
        {
            var files = await localFolder.GetFilesAsync();

            foreach (var file in files)
            {
                if (file.Name == databaseName)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
