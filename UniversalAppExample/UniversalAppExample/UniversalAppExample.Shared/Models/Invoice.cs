﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;
using SQLiteNetExtensions.Attributes;
using UniversalAppExample.Models.Relationships;

namespace UniversalAppExample.Models
{
    public class Invoice
    {   
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        [Unique]
        public string InvoiceNumber { get; set; }

        public DateTime Date { get; set; }
        public DateTime PaymentDate { get; set; }

        public decimal Value { get; private set; }

        private List<Product> _products { get; set; }
        [ManyToMany(typeof (InvoiceProduct), CascadeOperations = CascadeOperation.All)]
        public List<Product> Products
        {
            get { return _products; }
            set
            {
                _products = value;

                foreach (var product in _products)
                {
                    Value += product.Price;
                }
            }
        }

        [ForeignKey(typeof(Seller))]
        public int SellerId { get; set; }
        [ManyToOne(CascadeOperations = CascadeOperation.All)]
        public Seller Seller { get; set; }

        [ForeignKey(typeof(Seller))]
        public int CustomerId { get; set; }
        [ManyToOne(CascadeOperations = CascadeOperation.All)]
        public Customer Customer { get; set; }
    }
}
