﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using SQLite;
using SQLiteNetExtensions.Extensions;
using UniversalAppExample.Infrastructure.Data.Local;
using UniversalAppExample.Models;
using UniversalAppExample.Models.Relationships;

namespace UniversalAppExample.Infrastructure
{
    public static class SQLiteFacade
    {
        private static SQLiteConnection _sqLiteConnection { get; set; }
        private static readonly string DATABASE_FILE = "database.db";

        #region InitializationMethods

        public static async Task<bool> InitializeDatabase()
        {
            try
            {
                _sqLiteConnection = new SQLiteConnection(await DatabaseHelper.GetPathOfDatabaseInstance(DATABASE_FILE), true);

                CreateTables();

                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        private static void CreateTables()
        {
            if (!TableExists("Customers", _sqLiteConnection)) { _sqLiteConnection.CreateTable<Customer>(); }
            if (!TableExists("Sellers", _sqLiteConnection)) { _sqLiteConnection.CreateTable<Seller>(); }
            if (!TableExists("Invoices", _sqLiteConnection)) { _sqLiteConnection.CreateTable<Invoice>(); }
            if (!TableExists("Products", _sqLiteConnection)) { _sqLiteConnection.CreateTable<Product>(); }

            if (!TableExists("InvoiceProduct", _sqLiteConnection)) { _sqLiteConnection.CreateTable<InvoiceProduct>(); }
        }

        private static bool TableExists(String tableName, SQLiteConnection sqliteConnection)
        {
            SQLite.SQLiteCommand cmd = new SQLite.SQLiteCommand(sqliteConnection);
            cmd.CommandText = "SELECT * FROM sqlite_master WHERE type = 'table' AND name = '" + tableName + "'";
            var results = cmd.ExecuteQuery<object>();

            if (results.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        #endregion


        #region CRUD

        public static List<Customer> GetCustomers()
        {
            return _sqLiteConnection.GetAllWithChildren<Customer>(recursive: true);
        }

        public static bool AddCustomers(List<Customer> customers)
        {
            try
            {
                _sqLiteConnection.InsertAllWithChildren(customers, recursive: true);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }


        public static List<Seller> GetSellers()
        {
            return (from s in _sqLiteConnection.Table<Seller>() orderby s.Id select s).ToList();
        }

        public static bool AddSellers(List<Seller> sellers)
        {
            try
            {
                _sqLiteConnection.InsertAllWithChildren(sellers);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }


        public static List<Invoice> GetInvoices()
        {
            return _sqLiteConnection.GetAllWithChildren<Invoice>(recursive: true);
        }

        public static bool AddInvoices(List<Invoice> invoices)
        {
            try
            {
                _sqLiteConnection.InsertAllWithChildren(invoices, recursive: true);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool RemoveInvoice(int invoiceId)
        {
            try
            {
                _sqLiteConnection.Delete<Invoice>(invoiceId);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }


        public static List<Product> GetProducts()
        {
            return _sqLiteConnection.GetAllWithChildren<Product>(recursive: true);
        }

        public static bool AddProducts(List<Product> products)
        {
            try
            {
                _sqLiteConnection.InsertAllWithChildren(products, recursive: true);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }



        #endregion



    }
}
