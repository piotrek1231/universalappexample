﻿using System;
using System.Collections.Generic;
using System.Text;
using Windows.ApplicationModel.Resources;

namespace UniversalAppExample.Infrastructure.Resources
{
    public static class Localization
    {
        private static ResourceLoader resourceLoader;

        static Localization()
        {
            resourceLoader = new ResourceLoader();
        }

        public static string GetString(LocalizedStringEnum resource)
        {
            return resourceLoader.GetString(resource.ToString());
        }
    }


    public enum LocalizedStringEnum
    {
        ApplicationName,
        StartPageHeaderText,
        StartPageWelcomeMessage,
        StartPageAppBarButtonLabelNavigateToMainPage,
        MainPageHeaderText
    }




}
