﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLiteNetExtensions.Attributes;

namespace UniversalAppExample.Models.Relationships
{
    public class InvoiceProduct
    {
        [ForeignKey(typeof(Invoice))]
        public int InvoiceId { get; set; }

        [ForeignKey(typeof(Product))]
        public int ProductId { get; set; }
    }
}
