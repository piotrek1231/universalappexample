﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UniversalAppExample.Interfaces
{
    public interface INavigationService
    {
        void Navigate(Type sourcePageType);
        void Navigate(Type sourcePageType, object parameter);
        void GoBack();
    }
}
