﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;
using SQLiteNetExtensions.Attributes;
using UniversalAppExample.Models.Relationships;

namespace UniversalAppExample.Models
{
    public class Product
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        [MaxLength(10)]
        public string Symbol { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }

        [ManyToMany(typeof(InvoiceProduct), CascadeOperations = CascadeOperation.All)]
        public List<Invoice> Invoices { get; set; }
    }
}
