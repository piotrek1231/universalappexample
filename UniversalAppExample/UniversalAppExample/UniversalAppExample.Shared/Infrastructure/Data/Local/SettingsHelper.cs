﻿using System;
using System.Collections.Generic;
using System.Text;
using Windows.Storage;

namespace UniversalAppExample.Infrastructure.Data.Local
{
    public static class SettingsHelper
    {
        private static ApplicationDataContainer localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;


        public static bool SetProperty(Property property, object value)
        {
            try
            {
                switch (property)
                {
                    case Property.IsFirstRun:
                        localSettings.Values[GetValueFromEnum(Property.IsFirstRun)] = value;
                        break;

                    case Property.UserName:
                        localSettings.Values[GetValueFromEnum(Property.UserName)] = value;
                        break;
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static dynamic GetValueOfProperty(Property property)
        {
            try
            {
                if (!localSettings.Values.ContainsKey(GetValueFromEnum(property)))
                {
                    return string.Empty;
                }
                else
                {
                    switch (property)
                    {
                        case Property.IsFirstRun:
                            return localSettings.Values[GetValueFromEnum(Property.IsFirstRun)];
                            break;

                        case Property.UserName:
                            return localSettings.Values[GetValueFromEnum(Property.UserName)];
                            break;
                        default:
                            return string.Empty;
                            break;
                    }                    
                }
            }
            catch (Exception)
            {
                return null;
            }            
        }


        private static string GetValueFromEnum(Property property)
        {
            return Enum.GetName(typeof(Property), Property.IsFirstRun);
        }
    }




    public enum Property
    {
        IsFirstRun,
        UserName
    }



}
