﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using Windows.Foundation.Metadata;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using UniversalAppExample.Infrastructure;
using UniversalAppExample.Infrastructure.Data.Local;
using UniversalAppExample.Infrastructure.Resources;
using UniversalAppExample.Infrastructure.Services;
using UniversalAppExample.Interfaces;
using UniversalAppExample.Models;
using UniversalAppExample.Views;

namespace UniversalAppExample.ViewModels
{
    public class MainPageViewModel : ViewModelBase
    {
        INavigationService _navigationService;

        public MainPageViewModel(INavigationService navigationService)
        {
            _navigationService = navigationService;
            SelectInvoiceCommand = new RelayCommand<Invoice>((s) => SelectInvoice(s));
            RemoveInvoiceCommand = new RelayCommand<Invoice>((s) => RemoveInvoice(s));

            InitializeDatabase();
        }

        #region Commands

        public RelayCommand<Invoice> SelectInvoiceCommand { get; private set; }
        private void SelectInvoice(Invoice invoice)
        {
        }

        public RelayCommand<Invoice> RemoveInvoiceCommand { get; private set; }
        private void RemoveInvoice(Invoice invoice)
        {
            InvoicesCollection.Remove(invoice);
            SQLiteFacade.RemoveInvoice(invoice.Id);
        }

        #endregion



        #region Methods

        private async void InitializeDatabase()
        {


            if (await SQLiteFacade.InitializeDatabase())
            {
                List<Customer> customers = new List<Customer>();
                List<Seller> sellers = new List<Seller>();
                List<Product> products = new List<Product>();
                List<Invoice> invoices = new List<Invoice>();


                if (SQLiteFacade.GetCustomers().Count < 3)
                {
                    
                    customers.Add(new Customer()
                    {
                        Forename = "Tom",
                        Surname = "Brown",
                        DateOfBirth = new DateTime(1990, 11, 11),
                        LoyaltyPoints = 0,
                        CreditCardNumber = "AKM6583"
                    });
                    customers.Add(new Customer()
                    {
                        Forename = "Sarah",
                        Surname = "White",
                        DateOfBirth = new DateTime(1966, 2, 5),
                        LoyaltyPoints = 0,
                        CreditCardNumber = "TYP4663"
                    });
                    customers.Add(new Customer()
                    {
                        Forename = "Ricardo",
                        Surname = "Costa",
                        DateOfBirth = new DateTime(1993, 1, 1),
                        LoyaltyPoints = 20,
                        CreditCardNumber = "OPT9087"
                    });
                }

                if (SQLiteFacade.GetSellers().Count < 2)
                {
                    sellers.Add(new Seller()
                    {
                        Forename = "David",
                        Surname = "Falcao",
                        DateOfBirth = new DateTime(1967, 3, 9),
                        DateOfStartWork = new DateTime(2010, 4, 1),
                        InternalId = "WT23"
                    });
                    sellers.Add(new Seller()
                    {
                        Forename = "Monika",
                        Surname = "Kwiatkowska",
                        DateOfBirth = new DateTime(1985, 11, 13),
                        DateOfStartWork = new DateTime(2014, 11, 1),
                        InternalId = "CC76"
                    });
                }

                if (SQLiteFacade.GetProducts().Count < 5)
                {
                    products.Add(new Product()
                    {
                        Name = "Juice",
                        Price = 2.4m,
                        Symbol = "J01"
                    });
                    products.Add(new Product()
                    {
                        Name = "Pepsi",
                        Price = 3.2m,
                        Symbol = "P98"
                    });
                    products.Add(new Product()
                    {
                        Name = "Bread",
                        Price = 1.1m,
                        Symbol = "BR0"
                    });
                    products.Add(new Product()
                    {
                        Name = "Pizza",
                        Price = 12.4m,
                        Symbol = "P31"
                    });
                    products.Add(new Product()
                    {
                        Name = "Milk",
                        Price = 0.9m,
                        Symbol = "M91"
                    });
                }


                if (SQLiteFacade.GetInvoices().Count < 3)
                {
                    invoices.Add(new Invoice()
                    {
                        Customer = customers.Last(),
                        Seller = sellers.Last(),
                        Products = products,
                        PaymentDate = new DateTime(2015, 6, 6),
                        Date = new DateTime(2015, 2, 2),
                        InvoiceNumber = "2/2/2015-01"
                    });
                    invoices.Add(new Invoice()
                    {
                        Customer = customers.First(),
                        Seller = sellers.Last(),
                        Products = products.Where(i => i.Price < 1.0m).ToList(),
                        PaymentDate = new DateTime(2015, 3, 5),
                        Date = new DateTime(2015, 2, 2),
                        InvoiceNumber = "2/2/2015-02"
                    });
                    invoices.Add(new Invoice()
                    {
                        Customer = customers.FirstOrDefault(),
                        Seller = sellers.Last(),
                        Products = products.Where(i => i.Price > 1.0m).ToList(),
                        PaymentDate = new DateTime(2015, 2, 2),
                        Date = new DateTime(2015, 2, 2),
                        InvoiceNumber = "2/2/2015-03"
                    });

                    SQLiteFacade.AddInvoices(invoices);
                }
                InitializeCollection();
            }
        }

        private void InitializeCollection()
        {
            InvoicesCollection = new ObservableCollection<Invoice>(SQLiteFacade.GetInvoices());
        }

        #endregion

        #region Getters and Setters

            #region Collections

            private ObservableCollection<Invoice> _invoicesCollection;
            public ObservableCollection<Invoice> InvoicesCollection
            {
                get { return _invoicesCollection; }
                set { _invoicesCollection = value; RaisePropertyChanged("InvoicesCollection"); }
            }

            #endregion

            #region Localization

            public string HeaderText
            {
                get
                {
                    return Localization.GetString(LocalizedStringEnum.MainPageHeaderText);
                }
            }

            #endregion


        #endregion

    }
}
